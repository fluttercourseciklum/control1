import 'package:control1/models/random_user.dart';
import 'package:control1/pages/user_detail_page.dart';
import 'package:flutter/material.dart';

class UserSelect extends StatelessWidget {
  const UserSelect({Key key, @required this.user})
      : assert(user != null),
        super(key: key);

  final RandomUser user;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: <Widget>[
        Image.network(user.image),
        Text(user.name),
        IconButton(icon: Icon(Icons.rotate_right), onPressed: () {}),
        IconButton(
            icon: Icon(Icons.chevron_right),
            onPressed: () {
              Navigator.push<dynamic>(
                  context,
                  MaterialPageRoute<dynamic>(
                    builder: (context) => UserDetailPage(user),
                  ));
            })
      ],
    ));
  }
}
