
class RandomUser {
  RandomUser(this.name, this.image, this.lat, this.lng);

  // Need convert temp to int, because sometimes Open weather returns double
  RandomUser.fromJson(Map<String, dynamic> json)
      : name = json['results'][0]['name']['first'] + ' ' + json['results'][0]['name']['first'],
        lat = double.tryParse(json['results'][0]['location']['coordinates']['latitude']),
        lng = double.tryParse(json['results'][0]['location']['coordinates']['longitude']),
        image = json['results'][0]['picture']['large'];

  final String name;
  final String image;
  final double lat;
  final double lng;
}