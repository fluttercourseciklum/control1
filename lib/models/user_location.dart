import 'dart:ffi';

import 'package:geolocator/geolocator.dart';

class UserLocation {
  UserLocation(this.lat, this.lng);

  // Need convert temp to int, because sometimes Open weather returns double
  UserLocation.location(Position location)
      : lat = location.latitude.toDouble(),
        lng = location.longitude.toDouble();

  final double lat;
  final double lng;
}