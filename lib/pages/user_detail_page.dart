
import 'package:control1/models/random_user.dart';
import 'package:control1/models/user_location.dart';
import 'package:control1/providers/user_location_provider.dart';
import 'package:flutter/material.dart';

class UserDetailPage  extends StatelessWidget{

  const UserDetailPage(this.user);

  final RandomUser user;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child:  Image.network(user.image),
      ),
    );
  }

}