import 'package:control1/models/random_user.dart';
import 'package:control1/providers/random_user_provider.dart';
import 'package:control1/widget/user_select_widget.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final _randomUserProvider = RandomUserProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Almost tinder', textAlign: TextAlign.center),
          backgroundColor:  Colors.black12,
          brightness: Brightness.light,
        ),
        body: Center(
          
            child: FutureBuilder<RandomUser>(
                future: _randomUserProvider.getUser(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return UserSelect(user: snapshot.data);
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }
                  return const CircularProgressIndicator();
                })));
  }
}
