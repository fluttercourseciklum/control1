import 'dart:async';
import 'dart:convert';
import 'package:control1/models/random_user.dart';
import 'package:http/http.dart' as http;

class RandomUserProvider {
  Future<RandomUser> getUser() async {
    const api =
        'https://randomuser.me/api/';

    final response = await http.get(api);
    if (response.statusCode == 200) {
      return RandomUser.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load weather data');
    }
  }
}
