import 'dart:async';
import 'package:control1/models/user_location.dart';
import 'package:geolocator/geolocator.dart';

class UserLocationProvider {
  Future<UserLocation> getLocation() async {
    final position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    return UserLocation.location(position);
  }
}
